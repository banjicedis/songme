import React, { Component } from 'react';
import { SongMee } from '../lib/BackendFactory';
import { validateUsername, validateFirstName, validateLastName, validatePassword, validateConfirmPassword, validateEmail } from '../lib/helpers.js';

class Login extends Component {
  state = {
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    errors: {},
    hasLoginError: false,
    loginError: '',
  }
  render() {
    const { email, username, firstName, lastName, password, confirmPassword, errors, hasLoginError, loginError } = this.state;
    return (
      <div className="content">
        <h2>Sign Up</h2>
        <div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="email">Email</label>
            <input
              id="email"
              name="email"
              onChange={ event => this.handleChangeText(event, 'email') }
              type="email"
              value={ email } />
            { errors && errors.email && (<span className="errorhandling">{ errors.email }</span>)}
          </div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="username">Username</label>
            <input
              id="username"
              name="username"
              onChange={ event => this.handleChangeText(event, 'username') }
              type="text"
              value={ username } />
            { errors && errors.username && (<span className="errorhandling">{ errors.username }</span>)}
          </div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="firstName">First Name</label>
            <input
              id="firstName"
              name="firstName"
              onChange={ event => this.handleChangeText(event, 'firstName') }
              type="text"
              value={ firstName } />
            { errors && errors.firstName && (<span className="errorhandling">{ errors.firstName }</span>)}
          </div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="lastName">Last Name</label>
            <input
              id="lastName"
              name="lastName"
              onChange={ event => this.handleChangeText(event, 'lastName') }
              type="text"
              value={ lastName } />
            { errors && errors.lastName && (<span className="errorhandling">{ errors.lastName }</span>)}
          </div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="password">Password</label>
            <input
              id="password"
              name="password"
              onChange={ event => this.handleChangeText(event, 'password') }
              type="password"
              value={ password } />
            { errors && errors.password && (<span className="errorhandling">{ errors.password }</span>)}
          </div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="confirmPassword">Confirm Password</label>
            <input
              id="confirmPassword"
              name="confirmPassword"
              onChange={ event => this.handleChangeText(event, 'confirmPassword') }
              type="password"
              value={ confirmPassword } />
            { errors && errors.confirmPassword && (<span className="errorhandling">{ errors.confirmPassword }</span>)}
          </div>
          <div className="form-element form-checkbox">
            <input
              className="checkbox" id="confirm-terms" name="confirm"
              type="checkbox" value="yes" />
            <label htmlFor="confirm-terms">I agree to the <a href="/terms" target="_blank">Terms of Service</a> and <a href="/privacy" target="_blank">Privacy Policy</a></label>
          </div>
          <div className="form-element form-submit">
            {
              hasLoginError && (<p>{ loginError }</p>)
            }
            <button
              className="signup off"
              id="goLeft">
            Log In
            </button>
            <button
              className="signup"
              id="signUp"
              name="signup"
              onClick={ this.handleSubmit }>
              Sign up
            </button>
          </div>
        </div>
      </div>
    );
  }

  handleChangeText = (event, target) => this.setState({
    [target]: event.target.value,
    errors: {
      ...this.state.errors,
      [target]: false,
    },
  })

  handleSubmit = async() => {
    const { username, firstName, lastName, password, email, confirmPassword } = this.state;
    const errors = {
      username: validateUsername( username ),
      firstName: validateFirstName( firstName ),
      lastName: validateLastName( lastName ),
      password: validatePassword( password ),
      confirmPassword: validateConfirmPassword( password, confirmPassword ),
      email: validateEmail( email ),
    };
    if ( errors.username || errors.firstName || errors.lastName || errors.password || errors.email || errors.confirmPassword ) {
      return this.setState({ errors });
    }
    this.setState({ loading: true });
    const response = await SongMee.signup(email, username, password, firstName,  lastName);
    if ( !response.hasError ) {
      window.location.href = '/app';
      return true;
    }
    return this.setState({ hasLoginError: true, loginError: response.error });
  }
}

export default Login;
