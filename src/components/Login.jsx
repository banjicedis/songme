import React, { Component } from 'react';
import { SongMee } from '../lib/BackendFactory';
import { validateUsername, validatePassword } from '../lib/helpers.js';

class Login extends Component {
  state = {
    username: '',
    password: '',
    errors: {},
    hasLoginError: false,
    loginError: '',
  }
  render() {
    const { username, password, errors, hasLoginError, loginError } = this.state;
    return (
      <div className="content">
        <h2>Login</h2>
        <div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="username-login">Username</label>
            <input
              id="username-login"
              name="username"
              onChange={ event => this.handleChangeText(event, 'username') }
              type="text"
              value={ username } />
            { errors && errors.username && (<span className="errorhandling">{ errors.username }</span>)}
          </div>
          <div className="form-element form-stack">
            <label className="form-label" htmlFor="password-login">Password</label>
            <input
              id="password-login"
              name="password"
              onChange={ event => this.handleChangeText(event, 'password') }
              type="password"
              value={ password } />
            { errors && errors.password && (<span className="errorhandling">{ errors.password }</span>)}
          </div>
          <div className="form-element form-submit">
            {
              hasLoginError && (<p>{ loginError }</p>)
            }
            <button
              className="login"
              id="logIn"
              name="login"
              onClick={ this.handleSubmit }>
              Log In
            </button>
            <button
              className="login off" id="goRight"
              name="signup">
              Sign Up
            </button>
          </div>
          <div className="ForgetPasswordDiv">
            <a
              className="ForgetPasswordA"
              href="/forget-password">
            Forget password?
            </a>
          </div>
        </div>
      </div>
    );
  }

  handleChangeText = (event, target) => this.setState({
    [target]: event.target.value,
    errors: {
      ...this.state.errors,
      [target]: false,
    },
  })

  handleSubmit = async() => {
    const { username, password } = this.state;
    const errors = {
      username: validateUsername( username ),
      password: validatePassword( password ),
    };
    if ( errors.username || errors.password ) {
      return this.setState({ errors });
    }
    this.setState({ loading: true });
    const response = await SongMee.login(username, password);
    if ( !response.hasError ) {
      window.location.href = '/app';
      return true;
    }
    return this.setState({ hasLoginError: true, loginError: response.error });
  }
}

export default Login;
