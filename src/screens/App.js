import React, { Component } from 'react';
import android from '../assets/img/google.png';
import iphone from '../assets/img/apple.png';
import screen1 from '../assets/img/screen-1.png';
import screen2 from '../assets/img/screen-2.png';
import screen3 from '../assets/img/screen-3.png';
import screen4 from '../assets/img/screen-4.png';
import screen6 from '../assets/img/screen-8.png';


class App extends Component {
  render() {
    return (
      <div className="downloadpage">
        <div className="container">
          <div className="iPhone-5c">
            <div className="app-image">
              <img className="app-image" id="SlideOne" src={ screen1 } />
              <img className="app-image" id="SlideTwo" src={ screen2 } />
              <img className="app-image" id="SlideThree" src={ screen3 } />
              <img className="app-image" id="SlideFour" src={ screen4 } />
              <img className="app-image" id="SlideLast" src={ screen6 } />
            </div>
          </div>
          <div className="download">
            <h1>Never Be Without Music.</h1>
            <div className="downoad_image">
              <a href="#!"><img src={ android } /></a>
              <a href="#!"><img src={ iphone } /></a>
            </div>
            <p className="text_app"> Download APP now!</p>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
