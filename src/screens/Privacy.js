import React, { Component } from 'react';
import songme from '../assets/img/Songme.png';


class Privacy extends Component {
  render() {
    return (
      <div className="Terms__container">
        <div className="Terms__container-text">
          <h1 className="Terms__container-text-heading">Privacy Policy</h1>
          <p>It’s important to us that you feel comfortable and trust us with your information when you
          use the Carrubo d.o.o. (the “Carrubo”) services (the “Services”). Please take a few
          minutes to read this Privacy Policy, so that you understand what information we collect,
          what we do with it and why.</p>
          <p>In general, we only collect information that allows us to provide you with our best
          Services. This includes, for example, simple tasks like allowing other users to see the
          name and picture you choose to show. It also helps us to keep our Services clear of
          fraud and spam, and it allows us to get a unique understanding of what additional
          services may be useful to you, and all other purposes set out in this policy below.</p>
          <h2>Scope and consent</h2>
          <p>It’s important you understand that by using our Services, you give us consent to collect,
          use, disclose, and retain your personal information and other information but we will
          never read the content you are sharing privately. You can be sure that we will only use
          your information as described in this Policy.</p>
          <p>By using the Service, you are also agreeing to our Terms of Use. Please make sure you
          read and agree with our Terms of Use if you want to use Carrubo.</p>
          <h2>Information we collect</h2>
          <p>First of all, we want you to be assured that we do not read or listen to the content of your
          messages made privately via Carrubo.
          We collect the minimum information required to achieve the purposes set out in this
          Policy (see below), and you have the ability to limit such collection, as specified below
          under “Your Choices”:</p>
          <p><span className="underline">(a) Registration and Account Information:</span> When you use our various Services you
          voluntarily give us personal information (e.g., name, email, birth date, phone number and,
          when necessary, billing information) and you are not anonymous to us. That means your
          name and photo (if you choose to provide them) will be visible to other Carrubo users.</p>
          <p><span className="underline">(b) Social Media Information:</span> If you sign in to your Carrubo account through third-party
          social media sites like Facebook or Twitter, you agree to give us on-going access to your
          personal information on such sites (e.g., your public profile, friend list, accounts you
          follow or who follow you, your email address, birthday, work history, education history,
          interests, current city, and video viewing). We may receive certain information about you
          which is stored on social media sites if users of those sites give us access to their profiles
          and you are one of their friends or connections, depending upon your settings on those
          sites.</p>
          <p><span className="underline">(c) Activity Information:</span> We will receive information (e.g. the content you have viewed and interacted with) from
          your use of our Services. When you interact with Public Accounts on our Service, we
          may obtain information about the messages you have liked, comments you have left and
          also websites you’ve viewed through links from a Public Account. In addition, we collect information about the accounts you have visited and the content you have viewed in
          order to improve the relevance of our Services.</p>
          <p>We collect information about the value added services you are using over Carrubo and/or
          apps you have downloaded through Carrubo. This includes whether you are presently
          online, your personal preferences and the way you use that service. We may also tell
          other Carrubo users that you are using a certain service or app (to recommend them to
          try that service as well).</p>
          <p><span className="underline">(d) Information from Other Sources:</span> The information we collect may be combined with
          information from outside records (e.g. demographic information and additional contact
          information) that we have received in accordance with the law.</p>
          <p><span className="underline">(e) Additional Information:</span> We may collect additional information if you access our App
          through a specific device (e.g. your mobile device’s unique identifier; information about
          your device’s operating system, your browser, browser or operating system language;
          your wireless network, and your mobile carrier). We may also collect your WPS location
          data – you can choose whether to allow this by changing your geolocation tracking
          settings.</p>
          <h2>Specific Accounts –Email Linked to Carrubo Account</h2>
          <p><span className="underline">Linking your email to Carrubo:</span> linking your email to Carrubo allows you to connect to your
          Carrubo account from different devices, using your email and password only. When you
          select to connect your email to your Carrubo account, we will use information you
          provided in the registration process (e.g., name, email, password, phone number), as
          well as your IP address to do so.</p>
          <h2>Uses and Retention:</h2>
          <p>The information we collect from you ensures you can use the Carrubo Service and can
          be contacted on Carrubo. Your information is retained for as long as it is needed and
          helps us in our mission to constantly improve our Services and provide you with new
          experiences. As part of this mission, we may use your information for the following
          purposes:</p>
          <p><span className="underline">(a) Make our service available:</span> We use your Registration and Account information to (i)
          register you for the App and create your User Account for Carrubo; (ii) create your profile
          and make it visible; (iii) process your payments; (iv) create your Carrubo ID; (v) provide
          customer service, give you information about your account, and respond to your
          requests; (vi) personalize your experience by providing content on the Service, including
          targeted advertising of Carrubo services and other 3rd party services that we believe may
          be of most interest to you.</p>
          <p><span className="underline">(b) Improve our Services:</span> We use usage information (as described above) to better
          understand network behaviour and trends, detect potential outages and technical issues
          to improve our Services. We use aggregate information about our users and non-
          personal information to analyze our Services and user behaviour and prepare
          aggregated reports.</p>
          <p><span className="underline">(c) Provide Interesting Offerings to You:</span> As part of value added services provided by us
          or by a third party within Carrubo, we may use your information to continuously optimize
          and personalize those services and send you personal messages about new offerings
          which we believe you will find relevant (as long as you agreed on Carrubo or elsewhere
          to receive information from that service).</p>
          <p>Sending a message to a Public Account, or following a Public Chat on Carrubo will allow
          admins of that Public Account to send you notifications and personal messages. If you do
          not wish to receive such notifications, you may adjust your account settings to decline
          them or opt-out at any time from receiving further notifications. We may also use WPS
          location data to provide location-based offerings to you from our Service or third-party
          advertising. You will be asked to allow the use of your location data before Carrubo
          provides such offerings, and you may also adjust your account preferences to disable the
          use of your location data at any time.</p>
          <p><span className="underline">(d) Process Your Payments:</span> We may use your Information to process your payments for
          our Services through a third-party service provider.</p>

          <p><span className="underline">(e) Prevent Fraud &amp; Spam; enforcement of law:</span> We may use your information to prevent,
          detect, and investigate fraud, security breaches, potentially prohibited or illegal activities,
          protect our trademarks and enforce our Terms of Use. This may include URLs included in
          messages, which were reported as SPAM by other Users, or were otherwise suspected
          to be unsolicited. We may use your information to comply with applicable laws.</p>
          <p><span className="underline">(f) Communicate With You:</span> We may use your information to contact you (via message or
          other means) to maintain the App, to comply with your stated communication preferences
          or to provide updates about other Carrubo services.</p>
          <p>Unless otherwise specified, we retain information as long as it is necessary and relevant
          for us to achieve the purposes referred to above or to enable us to comply with our legal
          data protection retention obligations. You consent to our retention of your video viewing
          tracking data for as long as permissible under applicable law – up to one year after you
          close your account or otherwise withdraw consent for video tracking.</p>
          <h2>Disclosure:</h2>
          <p>We want you to know that we do not sell your personal information. We only share your
          personal information with third-parties outside of Carrubo, for the purposes listed above,
          in the following circumstances:</p>
          <p><span className="underline">(a) App Providers and Other Third-Parties:</span> We may disclose your information to service
          providers and other third-parties under contract who help with providing you our Services
          or other services provided by third-parties via our Services (such as, but not limited to,
          fraud and spam investigations, payment processing, site analytics and operations,
          providing special partnership features in our service, advertising – either on an aggregate
          non identifiable basis, or using unique identifier which can’t trace back to you). They are
          required to secure the data they receive, so that your personal information is guarded.</p>
          <p><span className="underline">(b) Other Entities with Your Consent:</span> We may disclose your information to other third-
          parties to whom you explicitly ask us to send your information within the use of integrated
          services (or about whom you are otherwise explicitly notified and consent to when using
          a specific service). On the Service, you may have opportunities to express interest in or
          register for other services. If you do, we will provide information about you to those third-
          parties, or parties working on their behalf, to implement your request. The transferred
          data will be governed by third party’s privacy policy.</p>
          <p><span className="underline">(c) Legal and Law Enforcement:</span> We may disclose your information to law enforcement,
          governmental agencies, or authorized third-parties, in response to a verified request
          relating to terror acts, criminal investigations or alleged illegal activity or any other activity
          that may expose us, you, or any other Carrubo user to legal liability.</p>

          <p><span className="underline">(d) Change of Control – New Owners:</span> We may share your information with another
          business entity, if we plan to merge with or be acquired by that business, or are involved
          in a transaction with similar financial effect. In such a situation we would make
          reasonable efforts to request that the new combined entity or other structure follow this
          Policy with respect to your personal information. If your personal information was
          intended to be used differently, we would ask the new entity to provide you with prior
          notice.</p>

          <h2>Information You Share Publicly</h2>
          <p>Some of our Services allow you to share information with others on a public basis. If you
          post information on a public feature of our Services or through social media sites, plug-
          ins or other applications, don’t forget this information is public on our Services and,
          depending upon your privacy settings, may also become public on the Internet. We can’t
          prevent or control further use of this information so please make sure you only post
          information that you’re happy to be seen publicly. We may also further use such public
          information you share, for various reasons, such as improving our service and providing
          you with relevant content, and analyzing trends.</p>
          <p>You can control what data you share through privacy settings available on some social
          media sites. Please refer to those third-party sites’ privacy policies and terms of use to
          learn more about their privacy practices, as we don’t control these.</p>
          <p>At this stage, whatever you share publicly is available to the entire Carrubo community. If
          you wish to remove a certain piece of content you have shared in the past, you can do
          that by deleting that content. It will be removed from our services but may remain on local
          devices of some users (assuming they have chosen to save it).</p>
          <h2>Your Choices</h2>
          <p>We want you to have control over how you communicate. You can control your privacy
          settings within the App to change the visibility of your online status. You can choose not
          to share your photo, opt not to receive location-based messages along with many other
          additional options, which are available within the app.</p>
          <p>Every now and then, Carrubo may send you notifications about administration and
          operation of the Service (for example, about your transactions, policy changes, technical
          issues, etc.). We may also send notifications about offerings by Carrubo or third-parties,
          that we think may be of interest to you. If you do not wish to receive such notifications,
          you may adjust your system settings to decline them (if you’re using iOS systems) or opt-
          out at any time from receiving further notifications by not using our service.</p>
          <h2>Cookies and Tracking Technologies</h2>
          <p>When you visit the App and/or our Website, we and our business partners may use
          cookies and other tracking technologies for a variety of purposes. These help to enhance
          your online experience, for example, by remembering your log-in status and viewing
          preferences from a previous use of our App when you return later. If you block cookies
          and tracking technologies you may not be able to access certain portions of our App. You
          may browse our site without telling us who you are, and we will obtain information about
          your computer or device (e.g., IP addresses) for the purpose of monitoring and improving
          our site and Services.</p>
          <p>Here are a few additional important things you should know about our use of tracking
          technologies (e.g., cookies, HTML-5 stored technologies):</p>
          <ul>
            <li>We ask for your prior consent to use cookies and tracking technologies policies
            when you join the App</li>
            <li>We offer certain features that are available only through the use of tracking
            technologies.</li>
            <li>We use both session and persistent tracking technologies. Tracking technologies
            (e.g., cookies) can either be persistent (i.e. they remain on your computer until
            you delete them) or temporary (i.e. they last only until you close your browser, or
            until you exit the App). You are always free to decline tracking technologies if
            your browser permits, although doing so may interfere with your use of the App
            and Website. We suggest you refer to the help section of your browser, browser
            extensions, or installed applications for instructions on blocking, deleting, or
            disabling tracking technologies such as cookies.</li>
            <li>We encode and protect the tracking technologies that Carrubo sets so that only
            we can interpret the information stored in them.</li>
            <li>You may come across tracking technologies/cookies from our third-party service
            providers  that we have allowed on our App to assist us with various aspects of
            our App operations and services, such as Google Analytics.</li>
            <li>You also may encounter tracking technologies from third-parties on certain pages
            of websites that we do not control and have not authorized. (For example, if you
            view a web page created by another user, there may be a cookie placed by that
            web page.)</li>
          </ul>

          <h2>Accessing, Reviewing, and Changing Your Personal Information</h2>
          <p>As a registered member, you can review and change personal information at any time by
          accessing your account on the App. Please be sure to update your personal information
          promptly if it changes or becomes inaccurate. We may retain some information from
          closed accounts so that we can comply with law, prevent fraud, assist with investigations,
          resolve disputes, analyze, or troubleshoot programs, enforce our Terms of Use, or take
          other actions permitted by law. Likewise, if your account or membership is terminated or
          suspended, we may maintain some information to prevent you from re-registering.
          Specific Location Practices: California, EU residents</p>
          <p className="underline">California Privacy Rights</p>
          <p>Residents of the State of California can request a list of all third-parties to which our App
          has disclosed certain personal information (as defined by California law) during the
          preceding year for those third-parties’ direct marketing purposes. If you are a California
          resident and want such a list, please contact us at <a href="mailto:CaliforniaRequest@songme.co">CaliforniaRequest@songme.co</a> . For
          all requests, please ensure you put the statement “Your California Privacy Rights” in the
          body of your request, as well as your name, street address, city, state, and zip code. In
          the body of your request, please provide enough information for us to determine if this
          applies to you. You need to attest to the fact that you are a California resident and
          provide a current California address for our response. Please note that we will not accept
          requests via the telephone, mail, or by facsimile, and we are not responsible for notices
          that are not labeled or sent properly, or that do not have complete information. Carrubo
          does not currently take actions to respond to Do Not Track signals because a uniform
          technological standard has not yet been developed. We continue to review new
          technologies and may adopt a standard once one is created.</p>
          <p className="underline">EU Privacy Rights</p>

          <p>If you are resident in the European Economic Area, you may have certain rights, in
          accordance with applicable laws, concerning your personal data, including the right to
          request to access your personal data; to have your personal data corrected or erased or
          restricted; and the right to object to the use of your personal data for marketing purposes.</p>
          <h2>Third-Party websites:</h2>
          <p>Our app may contain links to other third-party websites or you may access Apps from a
          third- party site. We are not responsible for the privacy practices or the content of these
          third-party sites.</p>
          <h2>Security</h2>
          <p>We know that security is important to our users and we care about the security of your
          information. We maintain technical, physical, and administrative security measures to
          protect the security of your personal information against loss, misuse, unauthorized
          access, disclosure, or alteration. Some of the safeguards we use include firewalls, data
          encryption, physical access controls to our data centers and information access
          authorization controls. We need your help too: it is your responsibility to make sure that
          your personal information is accurate and that your password(s) and account registration
          information are secure and not shared with third-parties.</p>
          <h2>Children’s Privacy</h2>
          <p>Our Services are not intended for children under the age of 13. Therefore we do not
          knowingly collect personal information via our websites, applications, services, or tools
          from anyone under 13.</p>
          <h2>International Transfer</h2>
          <p>We operate internationally and provide our Services to Carrubo users worldwide allowing
          them to communicate with each other across the globe. That means that your personal
          information may need to be processed in countries where data protection and privacy
          regulations may not offer the same level of protection as in your home country. We store
          and process your personal information on our computers in the United States, Asia,
          Europe (including Russia) and in other locations and use service providers that may be
          located in various locations outside of the European Economic Area (EEA).</p>
          <h2>Updates to this Policy</h2>
          <p>From time to time, as our services evolve, we may update this Policy. You agree that we
          may notify you about material changes in the way we treat personal information by
          placing a notice on the App. Please check the App frequently for updates.</p>
          <h2>Contact Us</h2>
          <p>If you still have questions about our privacy policy, please feel free to send us an email to
            <a href="mailto:privacy@songme.co">privacy@songme.co</a> or using the contact us form on the App.</p>
          <p>In the event you read this Privacy Policy in any language other than English, you agree
          that in the event of any discrepancies, the English version shall prevail.</p>
        </div>
        <a href="/"><img
          alt="logo" className="Terms__logo" src={ songme }
          width="200px" /></a>
      </div>
    );
  }
}

export default Privacy;
