import React, { Component } from 'react';
import { SongMee } from '../lib/BackendFactory';
import Songme from '../assets/img/Songme.png';

class ForgetPassword extends Component {
  state = {
    email: '',
    mailGotten: false,
    hasError: false,
  }
  render() {
    const { mailGotten, hasError } = this.state;
    return (
      <div className="ForgetPassword">
        <img
          alt="Logo"
          className="ForgetPassword__inside--logo"
          src={ Songme } />
        {
          !mailGotten &&
          <div className="ForgetPassword__inside">
            <input
              className="ForgetPassword__inside--input"
              onChange={ e => this.setState({ email: e.target.value }) }
              placeholder="Type your email" type="email" />
            <button
              className="ForgetPassword__button"
              onClick={ this.handleSendEmail }>
              Send
            </button>
          </div>
        }
        {
          mailGotten && <p className="ForgetPassword__check">Check the mail</p>
        }
        {
          hasError &&
          <p className="ForgetPassword__wrong">Something went wrong</p>
        }
      </div>
    );
  }

  handleSendEmail = async() => {
    try {
      await SongMee.changePasswordRequest(this.state.email);
      this.setState({ mailGotten: true, hasError: false });
    } catch (e) {
      this.setState({ mailGotten: false, hasError: true });
    }
  }
}

export default ForgetPassword;
