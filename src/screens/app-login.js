import React, { Component } from 'react';
import songme from '../assets/img/Songme.png';
import Signup from '../components/Signup';
import Login from '../components/Login';


class AppLogin extends Component {
  render() {
    return (
      <div className="App">
        <div id="back">
          <div className="backRight">
            <img
              alt="logo"
              className="backLeft__logo"
              src={ songme }
              width="200px" />
          </div>
          <div className="backLeft">
            <div className="songmee" />
          </div>
        </div>
        <div id="slideBox">
          <div className="topLayer">
            <div className="left">
              <Signup />
            </div>
            <div className="right">
              <Login />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AppLogin;
