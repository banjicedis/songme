import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SongMee } from '../lib/BackendFactory';
import Songme from '../assets/img/Songme.png';


class ChangePassword extends Component {
  state = {
    password: '',
    confirmPassword: '',
    isConfirmTouched: false,
    passwordChanged: false,
    hasError: false,
  }
  componentWillMount() {
    const { match: { params: { userId, changePasswordToken } } } = this.props;

    if (!userId || !changePasswordToken) {window.location.href = '/forget-password';}
  }

  render() {
    const { password, confirmPassword, isConfirmTouched, hasError, passwordChanged } = this.state;
    const areEquals = password === confirmPassword;
    if (passwordChanged && !hasError) {
      return (
        <div>
          Password have been changed
        </div>
      );
    }
    return (
      <div className="ForgetPassword">
        <img
          alt="Logo"
          className="ForgetPassword__inside--logo"
          src={ Songme } />
        <div className="ForgetPassword__inside">
          <input
            className="ForgetPassword__inside--input"
            name="password" onChange={ this.handleOnChange } placeholder="Type New Password"
            type="password" />
          <input
            className="ForgetPassword__inside--input"
            name="confirmPassword" onChange={ this.handleOnChange } placeholder="Confirm Password"
            type="password" />
        </div>
        {
          !areEquals && isConfirmTouched &&
          <p  className="ForgetPassword__check">Password doesn't match</p>
        }
        { hasError && <p  className="ForgetPassword__check">Error changing the password</p> }
        <button
          className="ForgetPassword__button2"
          onClick={ this.handleChangePasswordClick }>Change Password</button>
      </div>
    );
  }

  handleOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
      isConfirmTouched: this.state.isConfirmTouched || e.target.name === 'confirmPassword',
    });
  }

  handleChangePasswordClick = async() => {
    const { password, confirmPassword } = this.state;
    const { match: { params: { userId, changePasswordToken } } } = this.props;
    if (password !== confirmPassword) {return;}
    console.log(password);
    try {
      await SongMee.changePasswordWithToken(changePasswordToken, userId, password);
      this.setState({ passwordChanged: true, hasError: false });
    } catch (e) {
      this.setState({ passwordChanged: false, hasError: true });
    }
  }
}

ChangePassword.propTypes = {
  match: PropTypes.object.isRequired,
};

export default ChangePassword;
