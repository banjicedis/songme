import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import App from './screens/App';
import AppLogin from './screens/app-login';
import Privacy from './screens/Privacy';
import Terms from './screens/Terms';
import ForgetPassword from './screens/ForgetPassword';
import ChangePassword from './screens/ChangePassword';

class Routes extends Component {
  componentDidMount() {
    console.info( 'Mounted router' );
  }
  render() {
    const history = createBrowserHistory();
    return (
      <Router history={ history }>
        <Switch location={ history.location }>
          <Route component={ App } path="/app" />
          <Route component={ Privacy } path="/privacy" />
          <Route component={ Terms } path="/terms" />
          <Route component={ ChangePassword } path="/forget-password/:userId/token/:changePasswordToken" />
          <Route component={ ForgetPassword } path="/forget-password" />
          <Route component={ AppLogin } path="/" />
        </Switch>
      </Router>
    );
  }
}

export default Routes;
