const EMAIL_REGEX = /\S+@\S+\.\S+/;

export const validateUsername = username => {
  if ( !username || username === ''  ) return 'Username is required!';
  return false;
};
export const validateFirstName = firstName => {
  if ( !firstName || firstName === ''  ) return 'First Name is required!';
  return false;
};
export const validateLastName = lastName => {
  if ( !lastName || lastName === ''  ) return 'Last Name is required!';
  return false;
};

export const validatePassword = password => {
  if ( !password || password === ''  ) return 'Password is required!';
  return false;
};

export const validateConfirmPassword = (password, confirmPassword) => {
  if ( !password || password === ''  ) return 'Password is required!';
  if (
    !confirmPassword
    || confirmPassword === ''
    || confirmPassword !== password
  ) return 'Passwords must match!';
  return false;
};

export const validateEmail = email => {
  if (!email.match(EMAIL_REGEX)) {
    return 'Invalid email';
  }
  return false;
};
