import axios from 'axios';

const API_URL = 'http://ec2-35-157-137-156.eu-central-1.compute.amazonaws.com:8000/api';
// const API_URL = 'http://localhost:8000/api';

class BackendFactory {
  constructor() {
    console.log( 'constructing backend' );
  }

  async login(username, password) {
    try {
      const response = await this._fetch('POST', '/users/signin', { username, password } );
      return response.data;
    } catch ( error ) {
      return {
        hasError: true,
        error: error.message,
      };
    }
  }

  async signup(email, username, password, firstName, lastName) {
    try {
      const response = await this._fetch('POST', '/users/', { email, username, password, firstName, lastName } );
      return response.data;
    } catch ( error ) {
      return {
        hasError: true,
        error: error.message,
      };
    }
  }

  async changePasswordRequest( email ) {
    return await this._fetch('POST', '/users/request/change-password', { email });
  }

  async changePasswordWithToken( changePasswordToken, userId, newPassword ) {
    return await this._fetch('PUT', '/users/request/change-password', { changePasswordToken, userId, newPassword });
  }

  _fetch(method, endpoint, body) {
    return axios({
      method,
      url: `${ API_URL }${ endpoint }`,
      data: body,
    });
  }
}

const SongMee = new BackendFactory();

export { SongMee };
